package me.flyray.auth;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.spring4all.swagger.EnableSwagger2Doc;

/**
 * Created by bolei
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("me.flyray.auth.mapper")
@EnableSwagger2Doc
public class FlyrayAuthBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(FlyrayAuthBootstrap.class, args);
    }
}
