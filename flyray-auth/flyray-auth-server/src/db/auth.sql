/*
SQLyog v10.2 
MySQL - 5.7.20-log : Database - flyray-targe-auth
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`flyray-targe-auth` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `flyray-targe-auth`;

/*Table structure for table `auth_client` */

DROP TABLE IF EXISTS `auth_client`;

CREATE TABLE `auth_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL COMMENT '服务编码',
  `secret` varchar(255) DEFAULT NULL COMMENT '服务密钥',
  `name` varchar(255) DEFAULT NULL COMMENT '服务名',
  `locked` char(1) DEFAULT NULL COMMENT '是否锁定',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_name` varchar(255) DEFAULT NULL COMMENT '创建人姓名',
  `crt_host` varchar(255) DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime DEFAULT NULL COMMENT '更新时间',
  `upd_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `upd_name` varchar(255) DEFAULT NULL COMMENT '更新姓名',
  `upd_host` varchar(255) DEFAULT NULL COMMENT '更新主机',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auth_client` */

insert  into `auth_client`(`id`,`code`,`secret`,`name`,`locked`,`description`,`crt_time`,`crt_user`,`crt_name`,`crt_host`,`upd_time`,`upd_user`,`upd_name`,`upd_host`,`attr1`,`attr2`,`attr3`,`attr4`,`attr5`,`attr6`,`attr7`,`attr8`) values (1,'flyray-gate','123456','flyray-gate','0','',NULL,'','','','2017-07-07 21:51:32','1','管理员','0:0:0:0:0:0:0:1','','','','','','','',''),(3,'flyray-admin','123456','flyray-admin','0','',NULL,NULL,NULL,NULL,'2017-07-06 21:42:17','1','管理员','0:0:0:0:0:0:0:1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'flyray-auth','123456','flyray-auth','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'flyray-config','fXHsssa2','flyray-config','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'flyray-demo-mybatis','bZf8yvj9','flyray-demo-mybatis','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'flyray-template','bZf8yvj8','flyray-template','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'flyray-biz','123456','flyray-biz','0','业务模块','2018-04-18 09:39:44','null','null','null','2018-04-18 09:39:44','null',NULL,'null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'flyray-crm-core','123456','flyray-crm-core','0','用户框架','2018-05-17 10:03:16','null','null','null','2018-05-17 10:03:16','null',NULL,'null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'flyray-pay-core','123456','flyray-pay-core','0','支付模块',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'flyray-mall-api','123456','flyray-mall-api','0','flyray-mall-api','2018-08-23 17:00:36','1112','admin','127.0.0.1','2018-08-23 17:00:36','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'flyray-biz-thymeleaf','123456','flyray-biz-thymeleaf','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'flyray-netty','123456','flyray-netty','0','flyray-netty','2018-10-30 19:09:45','1112',NULL,'127.0.0.1','2018-10-30 19:09:45','1112',NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `auth_client_service` */

DROP TABLE IF EXISTS `auth_client_service`;

CREATE TABLE `auth_client_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auth_client_service` */

insert  into `auth_client_service`(`id`,`service_id`,`client_id`,`description`,`crt_time`,`crt_user`,`crt_name`,`crt_host`,`attr1`,`attr2`,`attr3`,`attr4`,`attr5`,`attr6`,`attr7`,`attr8`) values (21,'4','5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'3','6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,'16','14',NULL,'2018-05-17 10:06:19','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,'14','16',NULL,'2018-05-17 10:06:19','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,'17','16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,'17','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,'14','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,'3','16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,'16','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(58,'1','-1',NULL,'2018-08-23 17:01:18','1112',NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(59,'18','-1',NULL,'2018-08-23 17:01:18','1112',NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(60,'3','1',NULL,'2018-08-23 17:02:07','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(61,'6','1',NULL,'2018-08-23 17:02:07','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(62,'18','1',NULL,'2018-08-23 17:02:07','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(63,'3','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(64,'17','19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(66,'16','19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(67,'20','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `gateway_api_define` */

DROP TABLE IF EXISTS `gateway_api_define`;

CREATE TABLE `gateway_api_define` (
  `id` varchar(150) DEFAULT NULL,
  `path` varchar(765) DEFAULT NULL,
  `service_id` varchar(150) DEFAULT NULL,
  `url` varchar(765) DEFAULT NULL,
  `retryable` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `strip_prefix` int(11) DEFAULT NULL,
  `api_name` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gateway_api_define` */

insert  into `gateway_api_define`(`id`,`path`,`service_id`,`url`,`retryable`,`enabled`,`strip_prefix`,`api_name`) values ('','/payCore/**','flyray-pay-core',NULL,0,1,1,'paycoreapi'),('1','/admin/**','flyray-admin','',0,1,1,'back'),('10','/mall/**','flyray-mall-api',NULL,0,1,1,'mallapi'),('11','/doc/admin/**','flyray-admin',NULL,0,1,1,'admin-doc'),('12','/doc/auth/**','flyray-auth',NULL,0,1,1,'auth-car'),('2','/auth/**','flyray-auth','',0,1,1,'auth'),('3','/crm/**','flyray-crm-core',NULL,0,1,1,'crmapi'),('4','/biz/**','flyray-biz',NULL,0,1,1,'bizapi'),('5','/adminCrm/**','flyray-crm-core',NULL,0,1,1,'admincrmapi'),('7','/adminBiz/**','flyray-biz',NULL,0,1,1,'adminbizapi'),('8','/adminPayCore/**','flyray-pay-core',NULL,0,1,1,'adminpaycoreapi'),('9','/adminmall/**','flyray-mall-admin-api',NULL,0,1,1,'adminmallapi');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
