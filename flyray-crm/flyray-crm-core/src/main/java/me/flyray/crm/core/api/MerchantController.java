package me.flyray.crm.core.api;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.MerchantBase;
import me.flyray.crm.facade.request.MerchantBaseRequest;
import me.flyray.crm.facade.request.QueryMerchantInfoParam;
import me.flyray.crm.facade.request.QueryMerchantListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.ResponseHelper;
import me.flyray.crm.core.biz.merchant.MerchantBaseBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="商户管理")
@Controller
@RequestMapping("merchants")
public class MerchantController {
	
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 查询商户信息
	 * @param merchantBaseRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getMerchantInfo", method = RequestMethod.POST)
	public Map<String, Object> queryMerchantPassword(@RequestBody MerchantBaseRequest merchantBaseRequest){
		Map<String, Object> beanToMap = EntityUtils.beanToMap(merchantBaseRequest);
		return  merchantBaseBiz.queryMerchantInfo(beanToMap);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//===============================================================================================================================
	/**
	 * 微信小程序商户信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("微信小程序商户信息查询")
	@RequestMapping(value = "/list",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryFireSourceInfo(@RequestBody @Valid QueryMerchantListParam param) throws Exception {
		List<MerchantBase> list = merchantBaseBiz.queryList(param);
		return ResponseHelper.success(list, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
    }
	
	/**
	 * 查询单个客户基本信息
	 */
	@ApiOperation("微信小程序单个商户信息查询")
	@RequestMapping(value = "/info", method = RequestMethod.POST)
    @ResponseBody
	public MerchantBase queryInfo(@RequestBody @Valid QueryMerchantInfoParam param){
		return merchantBaseBiz.queryInfo(param.getMerchantId());
	}
	
}
