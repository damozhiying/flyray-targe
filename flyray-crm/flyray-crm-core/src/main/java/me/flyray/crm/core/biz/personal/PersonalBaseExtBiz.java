package me.flyray.crm.core.biz.personal;

import me.flyray.crm.core.entity.PersonalBaseExt;
import me.flyray.crm.core.mapper.PersonalBaseExtMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 个人基础信息扩展表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@Service
public class PersonalBaseExtBiz extends BaseBiz<PersonalBaseExtMapper, PersonalBaseExt> {
}