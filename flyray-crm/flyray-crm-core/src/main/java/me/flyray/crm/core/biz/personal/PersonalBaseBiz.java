package me.flyray.crm.core.biz.personal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import me.flyray.crm.facade.request.PersonalInfoRequest;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.MD5;
import me.flyray.common.util.Query;
import me.flyray.common.util.SnowFlake;
import me.flyray.common.vo.QueryPersonalBaseListRequest;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.CustomerBaseAuths;
import me.flyray.crm.core.entity.CustomerPushMsg;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalBaseExt;
import me.flyray.crm.core.entity.PersonalDistributionRelation;
import me.flyray.crm.core.entity.PersonalInfo;
import me.flyray.crm.core.mapper.CustomerBaseAuthsMapper;
import me.flyray.crm.core.mapper.CustomerBaseMapper;
import me.flyray.crm.core.mapper.CustomerPushMsgMapper;
import me.flyray.crm.core.mapper.PersonalBaseExtMapper;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.core.mapper.PersonalDistributionRelationMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 个人客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PersonalBaseBiz extends BaseBiz<PersonalBaseMapper, PersonalBase> {
	
	@Autowired
	private PersonalBaseExtMapper personalBaseExtMapper;
	@Autowired
	private PersonalDistributionRelationMapper personalDistributionRelationMapper;
	@Autowired
	private CustomerBaseMapper customerBaseMapper;
	@Autowired
	private CustomerBaseAuthsMapper customerBaseAuthsMapper;
	@Autowired
	private CustomerPushMsgMapper customerPushMsgMapper;
	
	/**
	 * 修改用户信息
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:28:59
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> modify(PersonalInfoRequest personalInfoRequest){
		log.info("【修改用户信息】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBaseReq = new PersonalBase();
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		personalBaseReq.setPlatformId(personalInfoRequest.getPlatformId());
		personalBaseReq.setPersonalId(personalInfoRequest.getPersonalId());
		personalBase = mapper.selectOne(personalBaseReq);
		log.info("【修改用户信息】   查询用户记录：{}",EntityUtils.beanToMap(personalBaseReq));
		if (personalBase != null) {
			
			//给对象id赋值，便于数据沉淀
			personalInfoRequest.setId(personalBase.getId());
			personalInfoRequest.setCustomerId(personalBase.getCustomerId());
			BeanUtils.copyProperties(personalInfoRequest, personalBase);
			BeanUtils.copyProperties(personalInfoRequest, personalBaseExt);
			log.info("【修改用户信息】   修改用户信息记录：{}",EntityUtils.beanToMap(personalBase));
			Map<String, Object> extMap=EntityUtils.beanToMap(personalBaseExt);
			log.info("【修改用户信息】   修改用户扩展信息记录：{}",extMap);
			Example example = new Example(PersonalBase.class);
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("platformId", personalBase.getPlatformId());
			criteria.andEqualTo("personalId", personalBase.getPersonalId());
			mapper.updateByExampleSelective(personalBase, example);
			//判断是否有扩展信息，有扩展信息的时候再更新扩展表
			if(extMap!=null && extMap.keySet().size()>1){
				personalBaseExtMapper.updateByPrimaryKeySelective(personalBaseExt);
			}
			//客户基础信息表
			CustomerBase customerBase=new CustomerBase();
			//客户授权表
			CustomerBaseAuths customerBaseAuths=new CustomerBaseAuths();
			BeanUtils.copyProperties(personalInfoRequest, customerBase);
			customerBase.setId(null);//将id置空
			if (!StringUtils.isEmpty(personalInfoRequest.getCredential())) {
				String passwordSalt = ObjectUtils.toString((new Random().nextInt() * (99999 - 10000 + 1)) + 10000);
				String passwordCiphertext = MD5.md5(personalInfoRequest.getCredential() + passwordSalt);
				customerBase.setPasswordSalt(passwordSalt);
				customerBaseAuths.setCredential(passwordCiphertext);
			}
			Map<String, Object> customerBaseMap=EntityUtils.beanToMap(customerBase);
			log.info("【修改用户信息】   修改客户基础信息记录：{}",customerBaseMap);
			//判断是否有修改信息，有修改信息的时候再更新表
			if(customerBaseMap!=null && customerBaseMap.keySet().size()>3){//personalId、platformId、userType
				Example customerBaseExample = new Example(CustomerBase.class);
				Criteria customerBaseCriteria = customerBaseExample.createCriteria();
				customerBaseCriteria.andEqualTo("platformId", personalBase.getPlatformId());
				customerBaseCriteria.andEqualTo("personalId", personalBase.getPersonalId());
				customerBaseCriteria.andEqualTo("customerId", personalBase.getCustomerId());
				customerBaseMapper.updateByExampleSelective(customerBase, customerBaseExample);
			}
			//通过授权表修改用户密码
			if(personalInfoRequest.getCredential()!=null && !"".equals(personalInfoRequest.getCredential())){
				Example customerBaseAuthsExample = new Example(CustomerBaseAuths.class);
				Criteria customerBaseAuthsCriteria = customerBaseAuthsExample.createCriteria();
				customerBaseAuthsCriteria.andEqualTo("platformId", personalBase.getPlatformId());
				customerBaseAuthsCriteria.andEqualTo("customerId", personalBase.getCustomerId());
				log.info("【修改用户信息】   修改客户授权信息记录：{}",EntityUtils.beanToMap(customerBaseAuths));
				customerBaseAuthsMapper.updateByExampleSelective(customerBaseAuths, customerBaseAuthsExample);
			}
			//客户消息推送参数表
			CustomerPushMsg customerPushMsg=new CustomerPushMsg();
			BeanUtils.copyProperties(personalInfoRequest, customerPushMsg);	
			customerPushMsg.setId(null);//将id置空
			Map<String, Object> customerPushMsgMap=EntityUtils.beanToMap(customerPushMsg);
			log.info("【修改用户信息】   修改客户消息推送参数表信息记录：{}",customerPushMsgMap);
			//判断是否有修改信息，有修改信息的时候再更新表
			if(customerPushMsgMap!=null && customerPushMsgMap.keySet().size()>1){//platformId
				Example customerPushMsgExample = new Example(CustomerPushMsg.class);
				Criteria customerPushMsgCriteria = customerPushMsgExample.createCriteria();
				customerPushMsgCriteria.andEqualTo("platformId", personalBase.getPlatformId());
				customerPushMsgCriteria.andEqualTo("customerNo", personalBase.getCustomerId());
				customerPushMsgMapper.updateByExampleSelective(customerPushMsg, customerPushMsgExample);
			}
	        respMap.put("code", BizResponseCode.OK.getCode());
	        respMap.put("msg", BizResponseCode.OK.getMessage());
		} else {
			respMap.put("code", BizResponseCode.PER_NOTEXIST.getCode());
	        respMap.put("msg", BizResponseCode.PER_NOTEXIST.getMessage());
		}
		log.info("【修改用户信息】   响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 查询用户信息
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:28:59
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> queryInfo(PersonalInfoRequest personalInfoRequest){
		log.info("【查询用户信息】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBaseReq = new PersonalBase();
		BeanUtils.copyProperties(personalInfoRequest, personalBaseReq);
		
		List<PersonalBase> personalBases = mapper.select(personalBaseReq);
		if (null != personalBases && personalBases.size() > 0) {
			PersonalBase personalBase = personalBases.get(0);
			PersonalBaseExt personalBaseExt = personalBaseExtMapper.selectByPrimaryKey(personalBase.getPersonalId());
			//查询客户基础信息表 登录密码、登录密码状态、盐值等
			Example example = new Example(CustomerBase.class);
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("platformId", personalBase.getPlatformId());
			criteria.andEqualTo("personalId", personalBase.getPersonalId());
			criteria.andEqualTo("customerId", personalBase.getCustomerId());
			List<CustomerBase> customerBases=customerBaseMapper.selectByExample(example);
			if(null != customerBases && customerBases.size() > 0){
				CustomerBase customerBase=customerBases.get(0);
				respMap.put("customerBase", customerBase);
			}
			//通过授权表查询用户密码
			Example authExample = new Example(CustomerBaseAuths.class);
			Criteria authCriteria = authExample.createCriteria();
			authCriteria.andEqualTo("platformId", personalBase.getPlatformId());
			authCriteria.andEqualTo("customerId", personalBase.getCustomerId());
			List<CustomerBaseAuths> customerBaseAuthsList=customerBaseAuthsMapper.selectByExample(authExample);
			if(null != customerBaseAuthsList && customerBaseAuthsList.size() > 0){
				CustomerBaseAuths customerBaseAuths=customerBaseAuthsList.get(0);
				respMap.put("password", customerBaseAuths.getCredential());
			}
			//消息推送表
			Example customerPushExample = new Example(CustomerPushMsg.class);
			Criteria customerPushCriteria = customerPushExample.createCriteria();
			customerPushCriteria.andEqualTo("platformId", personalBase.getPlatformId());
			customerPushCriteria.andEqualTo("customerNo", personalBase.getCustomerId());
			List<CustomerPushMsg> customerPushMsgList=customerPushMsgMapper.selectByExample(customerPushExample);
			if(null != customerPushMsgList && customerPushMsgList.size() > 0){
				CustomerPushMsg customerPushMsg=customerPushMsgList.get(0);
				respMap.put("customerPushMsg", customerPushMsg);
			}
			respMap.put("personalBase", personalBase);
			respMap.put("personalBaseExt", personalBaseExt);
			respMap.put("code", BizResponseCode.OK.getCode());
	        respMap.put("msg", BizResponseCode.OK.getMessage());
		} else {
			respMap.put("code", BizResponseCode.PER_NOTEXIST.getCode());
	        respMap.put("msg", BizResponseCode.PER_NOTEXIST.getMessage());
		}
        
		log.info("【查询用户信息】   响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 用户信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:28:59
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> queryInfoList(PersonalInfoRequest personalInfoRequest){
		log.info("【用户信息列表查询】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", personalInfoRequest.getPlatformId());
		reqMap.put("personalId", personalInfoRequest.getPersonalId());
		reqMap.put("customerId", personalInfoRequest.getCustomerId());
		reqMap.put("realName", personalInfoRequest.getRealName());
		reqMap.put("authenticationStatus", personalInfoRequest.getAuthenticationStatus());
		reqMap.put("status", personalInfoRequest.getStatus());
		
		List<PersonalInfo> personalInfos = mapper.queryPersonalList(reqMap);
		
		respMap.put("personalInfos", personalInfos);
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【用户信息列表查询】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 指定平台的所有用户信息列表查询
	 * @author clm
	 * @time 创建时间:2018年12月21日
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> queryInfoListAll(PersonalInfoRequest personalInfoRequest){
		log.info("【指定平台的所有用户信息列表查询】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", personalInfoRequest.getPlatformId());		
		List<PersonalInfo> personalInfos = mapper.queryPersonalList(reqMap);		
		respMap.put("personalInfos", personalInfos);
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【指定平台的所有用户信息列表查询】   响应参数：{}",respMap);
		return respMap;
	}
	
	

	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryPersonalBaseListRequest queryPersonalBaseListRequest){
		log.info("【查询个人基础信息列表】   请求参数：{}",EntityUtils.beanToMap(queryPersonalBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryPersonalBaseListRequest.getPlatformId();
		String personalId = queryPersonalBaseListRequest.getPersonalId();
		String customerId = queryPersonalBaseListRequest.getCustomerId();
		String realName = queryPersonalBaseListRequest.getRealName();
		String authenticationStatus = queryPersonalBaseListRequest.getAuthenticationStatus();
		String status = queryPersonalBaseListRequest.getStatus();
		int page = queryPersonalBaseListRequest.getPage();
		int limit = queryPersonalBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", platformId);
		reqMap.put("personalId", personalId);
		reqMap.put("customerId", customerId);
		reqMap.put("realName", realName);
		reqMap.put("authenticationStatus", authenticationStatus);
		reqMap.put("status", status);
		
        List<PersonalInfo> list = mapper.queryPersonalList(reqMap);
        TableResultResponse<PersonalInfo> table = new TableResultResponse<PersonalInfo>(result.getTotal(), list);
        
        respMap.put("body", table);
        respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【查询个人基础信息列表】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加个人基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:52
	 * @param personalBaseRequest
	 * @return
	 */
	public Map<String, Object> add(PersonalBaseRequest personalBaseRequest){
		log.info("【添加个人基础信息】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		BeanUtils.copyProperties(personalBaseRequest,personalBase);
		BeanUtils.copyProperties(personalBaseRequest,personalBaseExt);
		String personalId = String.valueOf(SnowFlake.getId());
		personalBase.setPersonalId(personalId);
		personalBase.setAuthenticationStatus("00");
		personalBase.setStatus("00");
		mapper.insert(personalBase);
		
		personalBaseExt.setPersonalId(personalId);
		personalBaseExtMapper.insert(personalBaseExt);
		
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【添加个人基础信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询单个个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:16:08
	 * @param personalBaseRequest
	 * @return
	 */
	public Map<String, Object> getOneObj(String personalId){
		log.info("【查询单个个人客户信息】   请求参数：personalId:{}",personalId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("personalId", personalId);
        List<PersonalInfo> list = mapper.queryPersonalList(reqMap);
		if (list != null && list.size() > 0) {
			PersonalInfo personalInfo = list.get(0);
			respMap.put("personalInfo", personalInfo);
			respMap.put("code", BizResponseCode.OK.getCode());
	        respMap.put("msg", BizResponseCode.OK.getMessage());
		} else {
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
	        respMap.put("msg", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		}
		
		log.info("【查询单个个人客户信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 删除个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:27:03
	 * @param personalBaseReq
	 * @return
	 */
	public Map<String, Object> removeObj(String personalId){
		log.info("【删除个人客户信息】   请求参数：personalId:{}",personalId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		personalBase.setPersonalId(personalId);
		personalBaseExt.setPersonalId(personalId);
		mapper.delete(personalBase);
		personalBaseExtMapper.delete(personalBaseExt);
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【删除个人客户信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 更新个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:34:13
	 * @param personalBaseReq
	 * @return
	 */
	public Map<String, Object> updateObj(PersonalBaseRequest personalBaseRequest){
		log.info("【更新个人客户信息】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		BeanUtils.copyProperties(personalBaseRequest,personalBase);
		BeanUtils.copyProperties(personalBaseRequest,personalBaseExt);
		
		mapper.updateByPrimaryKeySelective(personalBase);
		personalBaseExtMapper.updateByPrimaryKeySelective(personalBaseExt);
		
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【更新个人客户信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询个人信息关系网
	 * 1、入参  personalid
	 * 2、查询父级个人信息
	 * 3、查询个人一级个人信息列表
	 * @author centerroot
	 * @time 创建时间:2018年8月28日上午9:54:09
	 * @param personalId
	 * @return
	 */
	public Map<String, Object> queryPeopleNetwork(PersonalBaseRequest personalBaseRequest){
		log.info("【查询个人信息关系网】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		PersonalDistributionRelation personalDistributionRelationReq = new PersonalDistributionRelation();
		personalDistributionRelationReq.setPlatformId(personalBaseRequest.getPlatformId());
		personalDistributionRelationReq.setPersonalId(personalBaseRequest.getPersonalId());
		personalDistributionRelationReq.setParentLevel(1);
		PersonalDistributionRelation personalDistributionRelation = personalDistributionRelationMapper.selectOne(personalDistributionRelationReq);
		if (null != personalDistributionRelation) {
			log.info("【查询个人信息关系网】   查询父级关系信息：{}",EntityUtils.beanToMap(personalDistributionRelation));
			PersonalBase personalBaseReq = new PersonalBase();
			personalBaseReq.setPlatformId(personalDistributionRelation.getPlatformId());
			personalBaseReq.setPersonalId(personalDistributionRelation.getParentPerId());
			PersonalBase parentPersonalBase = mapper.selectOne(personalBaseReq);
			log.info("【查询个人信息关系网】   查询父级个人信息：{}",EntityUtils.beanToMap(parentPersonalBase));
			respMap.put("parentPersonalBase", parentPersonalBase);
		}
		
		Map<String, Object> reqChild = new HashMap<>();
		reqChild.put("platformId", personalBaseRequest.getPlatformId());
		reqChild.put("personalId", personalBaseRequest.getPersonalId());
		List<PersonalBase> childPersonals = mapper.queryPersonalNetwork(reqChild);
		log.info("【查询个人信息关系网】   查询子级个人信息：{}",EntityUtils.beanToMap(childPersonals));
		respMap.put("childPersonals", childPersonals);
		
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("msg", BizResponseCode.OK.getMessage());
		log.info("【查询个人信息关系网】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 小程序用户新增
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramAdd(Map<String, Object> request){
		log.info("【小程序用户新增】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String customerId = (String) request.get("customerId");
		String avatar = (String) request.get("avatar");
		String nickName = (String) request.get("nickName");
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		PersonalBase selectPersonalBase = mapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			personalBase.setPersonalId(String.valueOf(SnowFlake.getId()));
			personalBase.setAvatar(avatar);
			personalBase.setNickName(nickName);
			mapper.insert(personalBase);
		}else{
			selectPersonalBase.setAvatar(avatar);
			selectPersonalBase.setNickName(nickName);
			mapper.updateByPrimaryKey(selectPersonalBase);
		}
		response.put("code", BizResponseCode.OK.getCode());
		response.put("msg", BizResponseCode.OK.getMessage());
		log.info("【小程序用户新增】   响应参数：{}", response);
		return response;
	}
	
	/**
	 * 小程序用户修改
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramUpdate(Map<String, Object> request){
		log.info("【小程序用户修改】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String customerId = (String) request.get("customerId");
		String avatar = (String) request.get("avatar");
		String nickName = (String) request.get("nickName");
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		PersonalBase selectPersonalBase = mapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			response.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
			response.put("msg", BizResponseCode.CUST_NOTEXIST.getMessage());
		}else{
			selectPersonalBase.setAvatar(avatar);
			selectPersonalBase.setNickName(nickName);
			mapper.updateByPrimaryKey(selectPersonalBase);
			response.put("code", BizResponseCode.OK.getCode());
			response.put("msg", BizResponseCode.OK.getMessage());
		}
		log.info("【小程序用户修改响应】   响应参数：{}", response);
		return response;
	}
	
	
	public Map<String, Object> savePersonalDistributionRelation(Map<String, Object> request){ 
		log.info("【小程序用户修改】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		
		
		return response;
	}
	
	
}