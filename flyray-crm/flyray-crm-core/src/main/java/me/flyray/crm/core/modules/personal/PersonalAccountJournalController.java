package me.flyray.crm.core.modules.personal;

import me.flyray.crm.core.entity.PersonalAccountJournal;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalAccountJournalBiz;

@RestController
@RequestMapping("personalAccountJournal")
public class PersonalAccountJournalController extends BaseController<PersonalAccountJournalBiz, PersonalAccountJournal> {

	private static final Logger logger= LoggerFactory.getLogger(PersonalAccountJournalController.class);
	
	/**
	 * 查询个人账户流水
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<PersonalAccountJournal> queryAccountJournal(@RequestBody customerAccountJournalRequest param){
		logger.info("查询个人账户流水。。。{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.queryPersonalAccountJournals(param);
	}
}