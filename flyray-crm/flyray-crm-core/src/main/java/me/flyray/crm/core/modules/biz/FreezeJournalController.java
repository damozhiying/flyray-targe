package me.flyray.crm.core.modules.biz;

import me.flyray.crm.core.entity.FreezeJournal;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.FreezeJournalBiz;


/**
 * 冻结流水表
 * @author Administrator
 *
 */
@RestController
@RequestMapping("freezeJournal")
public class FreezeJournalController extends BaseController<FreezeJournalBiz, FreezeJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(FreezeJournalController.class);
	/**
	 * 查询冻结流水表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<FreezeJournal> queryFreezeJournal(@RequestBody customerAccountJournalRequest param){
		logger.info("查询冻结流水表...{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.queryFreezeJournals(param);
	}
}