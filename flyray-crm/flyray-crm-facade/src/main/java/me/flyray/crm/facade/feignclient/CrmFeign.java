package me.flyray.crm.facade.feignclient;

import io.swagger.annotations.ApiOperation;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 11:37 2019/1/6
 * @Description: 调用crm 处理用户及账户信息
 */

@FeignClient(value = "flyray-crm-core")
public interface CrmFeign {

    /**
     * 业务系统统一注册成为会员
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/register",method = RequestMethod.POST)
    public BaseApiResponse<CustomerBaseResponse> register(@RequestBody RegisterRequest param);

    /**
     * 业务系统统一登陆
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/login",method = RequestMethod.POST)
    public BaseApiResponse<CustomerBaseResponse> login(@RequestBody LoginRequest param);

    /**
     * 商户绑卡
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/merchant/bindCard",method = RequestMethod.POST)
    public BaseApiResponse<Void> bindCard(@RequestBody MerchantBindCardRequest param);

    @RequestMapping(value = "feign/merchant/bindCard/update",method = RequestMethod.POST)
    public BaseApiResponse<Void> bindCardUpdate(@RequestBody MerchantBindCardRequest param);

    /**
     * 查询商户绑卡信息
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/merchant/bindCard/Info",method = RequestMethod.POST)
    public BaseApiResponse<CustomerBindCardResponse> bindCardInfo(@RequestBody MerchantBindCardRequest param);

    /**
     * 查询商户余额
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/accountQuery",method = RequestMethod.POST)
    public BaseApiResponse<List<CustomerAccountQueryResponse>> customerAccountQuery(@RequestBody AccountQueryRequest param);

    /**
     * 商户入账
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/intoAccount",method = RequestMethod.POST)
    public BaseApiResponse<Void> customerIntoAccount(@RequestBody IntoAccountRequest param);

    /**
     * 商户出账
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/outAccount",method = RequestMethod.POST)
    public BaseApiResponse<Void> customerOutAccount(@RequestBody OutAccountRequest param);

    /**
     * 商户余额冻结
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/freeze",method = RequestMethod.POST)
    public BaseApiResponse<FreezeJournalResponse> freeze(@RequestBody FreezeRequest param);

    /**
     * 商户余额解冻
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/unfreeze",method = RequestMethod.POST)
    public BaseApiResponse<UnfreezeJournalResponse> unfreeze(@RequestBody UnfreezeRequest param);

    /**
     * 个人或者商户解冻并出账的接口
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/customer/unfreezeAndOutAccount",method = RequestMethod.POST)
    public BaseApiResponse<Void> unfreezeAndOutAccount(@RequestBody UnfreezeAndOutAccountRequest param);


    /***
     * 个人或者商户解冻并转账接口
     * 适用于提现
     * */
    @RequestMapping(value = "feign/customer/unfreezeAndTransfer",method = RequestMethod.POST)
    public BaseApiResponse<Void> unfreezeAndTransfer(@RequestBody @Valid UnfreezeAndTransferRequest unFreezeAndTransferRequest);

    /***
     * 客户账户流水查询
     * */
    @RequestMapping(value = "feign/customer/accountJournalQuery",method = RequestMethod.POST)
    public BaseApiResponse<List<AccountJournalQueryResponse>> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest);


}
