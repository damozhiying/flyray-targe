package me.flyray.crm.facade.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 15:09 2019/1/11
 * @Description: 账户流水
 */

@Data
public class MerchantAccountOpJournalResponse {

    //流水号
    private String journalId;

    //平台编号
    private String platformId;

    //商户名称
    private String merchantName;

    //
    private String customerId;

    //商户编号
    private String merchantId;

    //
    private String fromAccountName;

    private String fromAccount;

    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    private String customerType;

    //账户类型
    private String accountType;

    //交易类型  01：充值，02：提现，
    private String tradeType;

    //订单号
    private String orderNo;

    //交易金额
    private BigDecimal tradeAmt;

    //
    private String toAccountName;

    private String toAccount;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;
}
