package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "回调地址查询请求参数")
public class PlatformCallbackUrlRequest implements Serializable {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

    //回调类型   00：支付回调  01：退款回调  02：消息通知
    @NotNull(message="回调类型不能为空")
	@ApiModelProperty(value = "回调类型")
    private String callbackType;
}
