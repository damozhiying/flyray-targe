package me.flyray.admin.feignserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.admin.biz.RoleBiz;
import me.flyray.admin.entity.Role;
import me.flyray.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/role")
public class FeignRole {
	@Autowired
	private RoleBiz roleBiz;
	
	@RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addRole(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result = roleBiz.add(param);
    		Role role = (Role) result.get("role");
    		Role roleOther =  new Role();
    		roleOther.setDeptId(role.getDeptId());
    		roleOther.setRemark(role.getRemark());
    		roleOther.setRoleName(role.getRoleName());
    		roleOther = roleBiz.selectOne(roleOther);
    		result.put("roleId", roleOther.getRoleId());
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", ResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
    	return result;
    }
}
