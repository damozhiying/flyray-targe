package me.flyray.admin.mapper;

import me.flyray.admin.entity.RoleMenu;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMenuMapper extends Mapper<RoleMenu> {
}