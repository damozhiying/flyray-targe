package me.flyray.common.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * @Author: bolei
 * @date: 9:24 2019/1/4
 * @Description: HmacSHA256Utils 工具类
 */

public class HmacSHA256Utils {

    /**
     * 将加密后的字节数组转换成字符串
     *
     * @param b 字节数组
     * @return 字符串
     */
    private static String byteArrayToHexString(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int n = 0; b!=null && n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1)
                hs.append('0');
            hs.append(stmp);
        }
        return hs.toString().toLowerCase();
    }
    /**
     * sha256_HMAC加密
     * @param message 消息
     * @param secret  秘钥
     * @return 加密后字符串
     */
    public static String sha256_HMAC(String message, String secret) {
        String hash = "";
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);



            byte[] bytes = sha256_HMAC.doFinal(message.getBytes());
            hash = byteArrayToHexString(bytes);
        } catch (Exception e) {
            System.out.println("Error HmacSHA256 ===========" + e.getMessage());
        }
        return hash;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(HmacSHA256Utils.sha256_HMAC("adSlotId=16846948780814336&adType=BANNER&appId=16753938326171648&appType=app&charset=utf-8&placement_id=16846948780814336&publisherId=16713335434457088&publisher_id=46964aeddfe512a4fb0b8789de20035c&sign_nonce=9fa156ce-4801-4bef-a2b7-22ccd885ec0f&sign_type=HmacSHA256&site=1&timestamp=2019-01-04 17:31:48&version=1.0&key=46964aeddfe512a4fb0b8789de20035c",
                "46964aeddfe512a4fb0b8789de20035c").toUpperCase());
    }


}
