package me.flyray.common.enums;

/**
 * 商户类型
 */
public enum MerchantType {

	MER_PLATFORM("MER00","企业平台"),
	MER_MERCHANT("MER01","企业商户"),
	MER_PERSONAL("MER02","个人商户");

    private String code;
    private String desc;

    private MerchantType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static MerchantType getSmsType(String code) {
        for (MerchantType o : MerchantType.values()) {
            if (o.getCode().equals(code)) {
                return o;
            }
        }
        return null;
    }
}
